CREATE TABLE `posjetitelji` (
  `posjetitelj_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`posjetitelj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `edukacije` (
  `edukacija_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `razina` int(11) NOT NULL,
  PRIMARY KEY (`edukacija_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `prostori` (
  `prostor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `edukacija_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`prostor_id`),
  KEY `prostori_edukacija_id_foreign` (`edukacija_id`),
  CONSTRAINT `prostori_edukacija_id_foreign` FOREIGN KEY (`edukacija_id`) REFERENCES `edukacije` (`edukacija_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `posjetitelj_edukacija` (
  `posjetitelj_id` bigint(20) unsigned NOT NULL,
  `edukacija_id` bigint(20) unsigned NOT NULL,
  `datum_polaganja` date NOT NULL,
  PRIMARY KEY (`posjetitelj_id`,`edukacija_id`,`datum_polaganja`),
  KEY `posjetitelj_edukacija_edukacija_id_foreign` (`edukacija_id`),
  CONSTRAINT `posjetitelj_edukacija_edukacija_id_foreign` FOREIGN KEY (`edukacija_id`) REFERENCES `edukacije` (`edukacija_id`),
  CONSTRAINT `posjetitelj_edukacija_posjetitelj_id_foreign` FOREIGN KEY (`posjetitelj_id`) REFERENCES `posjetitelji` (`posjetitelj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




CREATE TABLE `najava_dolaska` (
  `najava_dolaska_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `posjetitelj_id` bigint(20) unsigned NOT NULL,
  `prostor_id` bigint(20) unsigned NOT NULL,
  `datum_dolaska` date NOT NULL,
  PRIMARY KEY (`najava_dolaska_id`),
  KEY `najava_dolaska_posjetitelj_id_foreign` (`posjetitelj_id`),
  KEY `najava_dolaska_prostor_id_foreign` (`prostor_id`),
  CONSTRAINT `najava_dolaska_posjetitelj_id_foreign` FOREIGN KEY (`posjetitelj_id`) REFERENCES `posjetitelji` (`posjetitelj_id`),
  CONSTRAINT `najava_dolaska_prostor_id_foreign` FOREIGN KEY (`prostor_id`) REFERENCES `prostori` (`prostor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `dolasci` (
  `dolazak_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `najava_dolaska_id` bigint(20) unsigned NOT NULL,
  `vrijeme_dolaska` datetime NOT NULL,
  `vrijeme_odlaska` date NOT NULL,
  `potpis` longtext NOT NULL,
  `gdpr` tinyint(1) NOT NULL,
  PRIMARY KEY (`dolazak_id`),
  KEY `dolasci_najava_dolaska_id_foreign` (`najava_dolaska_id`),
  CONSTRAINT `dolasci_najava_dolaska_id_foreign` FOREIGN KEY (`najava_dolaska_id`) REFERENCES `najava_dolaska` (`najava_dolaska_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


















